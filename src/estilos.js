/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';

export default css`:

.caja{
  display:flex;
  flex-flow: row wrap;
  justify-content:center;
}
.a_100{
  width:100%;
}
.a_80{
  width:90%;
}
.caja2{
  display:flex;
  flex-flow: row wrap;
  justify-content:center;
  background-color:green;
}

.caja_vertical{
  display:flex;
  flex-flow: column wrap;
  align-items:center;
 
  background: linear-gradient(to bottom, rgb(26,121,207), rgb(3,50,98));
}

.red{
  background-color:red;
}

.azul{
  background-color:rgb(3,50,98);
}

`;