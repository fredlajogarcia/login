import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsLoginDm-styles.js';
import styles2 from './estilos.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-login-dm></cells-login-dm>
```

##styling-doc

@customElement cells-login-dm
*/
export class CellsLoginDm extends LitElement {
  static get is() {
    return 'cells-login-dm';
  }

  // Declare properties
  static get properties() {
    return {
      name: { type: String, },
      dni: { type: String, },
      password: { type: String, },

    };
  }

  // Initialize properties
  constructor() {
    super();
    this.name = 'Cells';
    this.dni = '';
    this.password = ''; 

  }

  static get styles() {
    return [
      styles,styles2,
      getComponentSharedStyles('cells-login-dm-shared-styles,estilos')
    ];
  }

  // Define a template
  render() {
    return html`

    <style>
        bbva-web-form-password.rojoo{
          --_field-bg-color: rgb(0,68,120);
          --_field-input-color:white;
          --_field-label-color:white;
          --_field-readonly-input-color:white;
          --_field-clear-color:white;
          --_field-focused-label-color:white;
        }
        .center {
          position: absolute;
          top: 15px;
          left: 50%;
          transform: translate(-50%, -50%);
          font-size: 15px;
          color :white;
        }
        .topleft {
          position: absolute;
          top: 8px;
          left: 10px;
          font-size: 12px;
          color :white;
        }
        .topright {
          position: absolute;
          top: 8px;
          right: 10px;
          font-size: 12px;
          color :white;
        }
        .bottom {
          position: absolute;
          bottom: 8px;
          font-size: 15px;
          color :white;
        }
        .circle{
          position:relative;
          top:-25px;
          border-radius:50%;
          width:50px;
          height:50px;
          background-color:rgb(0,68,120);
          color: white;
          display:flex;
          flex-flow:row wrap;
          justify-content:center;
          align-items:center;
        }
        .hidden{
          display:none;
        }


    </style>
      <slot></slot>
      <!--
      <bbva-web-form-select label="Tipo de Documento" @change=${this.setTipoDocu} style="color:green;">
        <bbva-web-form-option value="L">DNI</bbva-web-form-option>
        <bbva-web-form-option value="C">CE</bbva-web-form-option>
        <bbva-web-form-option value="R">RUC</bbva-web-form-option>
      </bbva-web-form-select>
      -->
      <div style="height:100%;" class="caja_vertical">
          <div class="a_100">
            <picture>
                <source media="(max-width: 450px)" srcset="https://www.exitoysuperacionpersonal.com/wp-content/uploads/2021/06/frases-de-faros.jpg">
                <source media="(min-width: 451px)" srcset="https://p0.pikist.com/photos/136/308/panoramic-sunset-dawn-nature-landscape-panorama-sky-travel-outdoors.jpg">
                <img src="https://www.exitoysuperacionpersonal.com/wp-content/uploads/2021/06/frases-de-faros.jpg" alt="defecto" width="100%">
              </picture>
          </div>
          <div class="circle" >FK</div>
          
          <p style="color:white; margin-top:0px;margin-bottom:5px;">Hola, Fred Kevin</p>
          <p style="color:white; margin:5px;font-size: 12px;margin-bottom:15px;">Cambiar Usuario</p>
          <div class="center">14:20</div>
          <div class="topleft">Movistar</div>
          <div class="topright">98%</div>
          <div class="bottom">Ingresar con huella digital</div>
          <div class="center" style="top: 45px;">
            <img src="https://bbvaassetmanagement.com/wp-content/uploads/sites/2/2019/10/BBVA_WHITE-1.png" alt="Parisaa" width="70px" >
          </div>
         
          <div class="case a_80">
            <bbva-web-form-password class ="rojoo" id="contra"  label="Ingresar Contraseña" @change=${this.setPassword} ></bbva-web-form-password>
          </div>
          <p style="color:white; margin:10px;font-size: 12px;">¿Olvidaste tu contraseña?</p>

          <bbva-core-generic-dp
            id="loginDP"
            host="https://tst-glomo.bbva.pe"
            path="SRVS_A02/TechArchitecture/pe/grantingTicket/V02"
            method="POST"  
            @request-success="${this.requestSuccess}" 
            @request-error="${this.requestError}"
          ></bbva-core-generic-dp>

          
          <bbva-web-button-default style="margin:10px; " @click=${this.getListPokemons} >Log in</bbva-web-button-default>
          <div id="exito" style="color:white;" class="hidden"> Login successfull !!!!</div>
          <div id="fallo" style="color:white;" class="hidden">Login failed</div>


          
          
          



      </div>
      
      


     

    `;
  }
  getListPokemons() {
    //bbva01
    this.password=this.shadowRoot.getElementById("contra").value;
    this.shadowRoot.getElementById("exito").classList.add('hidden');
    this.shadowRoot.getElementById("fallo").classList.add('hidden');
    console.log('intento login ...');
    let dp = this.shadowRoot.getElementById('loginDP');
    dp.body = {
              "authentication": {
              "userID": "L31158005",
              "consumerID": "13000013",
              "authenticationType": "02",
                "authenticationData": [
                {
                  "idAuthenticationData": "password",
                  "authenticationData": [
                    ""+this.password+""
                  ]
                }
              ]
            },
            "backendUserRequest": {
              "userId": "",
              "accessCode": "L31158005",
              "dialogId": ""
            }
          }
    dp.crossDomain =true;
    dp.generateRequest();

  }
  requestSuccess(evt){
    console.log('@requestSuccess',evt);
    this.shadowRoot.getElementById("exito").classList.remove('hidden');
    
  }

  requestError(evt){
    console.log('@requestError',evt);
    this.shadowRoot.getElementById("fallo").classList.remove('hidden');
  }
  setTipoDocu(e){
    console.log('setTipoDocu',e,e.target.value);
    this.dni = e.target.value;
  }

  setPassword(e){
    console.log('setPassword',e,e.target.value);
    
  }

  



}
