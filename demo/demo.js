import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import './css/demo-styles.js';
import '../cells-login-dm.js';
import '@bbva-web-components/bbva-core-generic-dp/bbva-core-generic-dp.js';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select.js';
import '@bbva-web-components/bbva-web-form-password/bbva-web-form-password.js';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';

// Include below here your components only for demo
// import 'other-component.js'
